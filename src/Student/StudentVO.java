package Student;

public class StudentVO {
	private int id;
	  private String Name;
	  private String Sex;
	  private String Country;
	  private int Age;
	  public int getId() {
	    return id;
	  }
	  public void setId(int id) {
	    this.id = id;
	  }
	  public String getName() {
	    return Name;
	  }
	  public void setName(String Name) {
		    this.Name = Name;
		  }
	  public String getSex() {
		    return Sex;
		  }
	  public void setSex(String Sex) {
		    this.Sex = Sex;
		  }

	  public String getCountry() {
	    return Country;
	  }
	  public void setCountry(String Country) {
	    this.Country = Country;
	  }
	  public int getAge() {
	    return Age;
	  }
	  public void setAge(int Age) {
	    this.Age = Age;
	  }
}

