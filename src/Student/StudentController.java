package Student;
import java.sql.SQLException;

public class StudentController {
 private StudentModel model;
 public StudentController() {
   model = new StudentModel();
 }
 public int insertStudent(StudentVO vo) {
   int cnt = 0;
   try {
     cnt = model.insert(vo);
   } catch (SQLException ex) {
      System.err.println("Error: " + ex.toString());
   }
   return cnt;
 }
 public StudentVO displayStudent(StudentVO emp) {
   StudentVO vo = null;
   try {
     vo = model.display(emp);
   } catch (SQLException ex) {
     System.err.println("Error: " + ex.toString());
   }
   return vo;
 }
}