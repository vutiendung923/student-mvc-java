package Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentModel {
 
 private static Connection getConnection() throws Exception {
   return DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=Student;user=sa;password=sa");
 }
 
 public int insert(StudentVO emp) throws SQLException {
   Connection c = null;
   PreparedStatement ps = null;
   int cnt = 0;
   try {
     c = this.getConnection();
     ps = c.prepareStatement("insert into Profile values( ?, ?, ?, ?)");
     int i = 0;
     ps.setNString(++i, emp.getName());
     ps.setNString(++i, emp.getSex());
     ps.setNString(++i, emp.getCountry());
     ps.setInt(++i, emp.getAge());
     cnt = ps.executeUpdate();
   } catch (Exception e) {
      e.printStackTrace();
   } finally {
     if (ps != null) {
       ps.close();
     }
     if (c != null) {
       c.close();
     }
   }
    return cnt;
 }
 
 public StudentVO display(StudentVO vo) throws SQLException {
   Connection con = null;
   PreparedStatement ps = null;
   ResultSet rs = null;
   StudentVO emp = null;
   int cnt = 0;
   try {
    con = this.getConnection();
    ps = con.prepareStatement("select * from Profile");
    rs = ps.executeQuery();
    emp = new StudentVO();
    while (rs.next()) {
      emp.setId(rs.getInt("Id"));
      emp.setName(rs.getString("Name"));
      emp.setSex(rs.getString("Sex"));
      emp.setCountry(rs.getString("Country"));
      emp.setAge(rs.getInt("Age"));
      cnt++;
    }
    if (cnt > 0)
      return emp;
   } catch (Exception e) {
     System.out.println(e);
   } finally {
     if (rs != null) {
       rs.close();
     }
     if (ps != null) {
       ps.close();
     }
     if (con != null) {
       con.close();
     }
   }
   return null;
 }
}