package Student;
import java.util.Scanner;

public class StudentView {
 private StudentVO vo;
 private StudentController controller;
 Scanner s;
 public StudentView() {
   controller = new StudentController();
   vo = new StudentVO();
   s = new Scanner(System.in);
 }
 public void createStudent() {
   System.out.print("Nhap ho ten sinh vien: ");
   vo.setName(s.nextLine());
   System.out.print("Nhap gioi tinh sinh vien: ");
   vo.setSex(s.nextLine());
   System.out.print("Nhap que quan: ");
   vo.setCountry(s.nextLine());
   System.out.print("Nhap tuoi: ");
   vo.setAge(s.nextInt());
   int cnt = controller.insertStudent(vo);
   if (cnt != 0) {
     System.out.println("Nhap vao thanh cong");
   } else {
     System.out.println("Khong the nh1ap");
   }
 }
 public void printStudentDetails() {
   System.out.print("Hien thi thong tin ");
   StudentVO emp = controller.displayStudent(vo);
   if (emp != null) {
     System.out.println(emp);
   } else {
   System.out.println("Chua co thong tin!");
  }
 }
 public static void main(String[] args) {
   Scanner s = new Scanner(System.in);
   int choice;
   StudentView view = new StudentView();
   do {
     System.out.println(" 1. Nhap thong tin sinh vien");
     System.out.println(" 2. Hien thi thong tin sinh vien");
     System.out.println(" 3. Thoat");
     System.out.print("Dien lua chon cua ban: ");
     choice = s.nextInt();
     s.nextLine();
     switch (choice) {
       case 1:
         view.createStudent();
         break;
       case 2:
         view.printStudentDetails();
         break;
       case 3:
    	   System.exit(0);   	 
    	   break;
     }
   } while (choice != 3);
 }
}
